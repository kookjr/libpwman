//    Copyright (C) 2016, 2017, 2021-2022 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <cctype>
#include <cmath>

#include <termios.h>
#include <unistd.h>
#include <tuple>
#include <array>

#include <pml/types.hpp>
#include <pml/io.hpp>
#include <pml/db.hpp>
#include <pml/crypto.hpp>
#include <pml/helpers.hpp>

#include "passphrase.hpp"
#include "revisionedfile.hpp"
#include "apphandler.hpp"

static const char* msg_usage =
    "pm is mainly an interactive command line program when used without\n"
    "parameters. Start pm without parameters and actions are\n"
    "presented.\n"
    "\n"
    "The HOME environment variable must be defined.\n"
    "\n"
    "Three interfactive command are available as shortcuts on the command\n"
    "line so they can be quickly used without providing the pass phrase to\n"
    "your password database. They are:\n"
    "    --help\n"
    "    --gen=pw_specification\n"
    "    --easy\n"
    "\n"
    "These will print this help message or generate a random password\n"
    "respectively.\n";
static const char* msg_help =
    "Commands:\n"
    "  quit\n"
    "    q\n"
    "  generate a random password\n"
    "    g[<pwLen>][l<num>][d<num][>s<num>]\n"
    "      pwLen - total length of password\n"
    "      l<num> - num of letters in password\n"
    "      d<num> - num of digits in password\n"
    "      s<num> - num of special characters in password\n"
    "    G[<pwLen>]\n"
    "      pwLen - suggested total length of password\n"
    "  add a new password entry\n"
    "    a\n"
    "  export password database, JSON format, to stdout or\n"
    "  or to optional file_name\n"
    "    e [file_name]\n"
    "  import password database from JSON format\n"
    "    i file_name\n"
    "  search for a password entry\n"
    "    s[afri] search_pattern\n"
    "      a - query all fields, not just name and location\n"
    "      f - search name of field too, not just value\n"
    "      r - regular expression search (not supported)\n"
    "      i - case insensitive search\n"
    "      <see sub-commands below>\n"
    "  help\n"
    "    ? or h\n"
    "\n"
    "After search, these sub-commands:\n"
    "  display or edit entry\n"
    "    [d]<num> - display the numbered entry\n"
    "    e<num>   - edit the numbered entry\n"
    "    x<num>   - delete the numbered entry\n";
static const char* version_footer =
    "\n"
    "Version: ";

static void pm_repl(pml::DB& db);
static void cmd_genpw(pml::DB& db, const std::string& instr);
static void cmd_easygenpw(pml::DB& db, const std::string& instr);
static void cmd_addpw(pml::DB& db, const std::string& instr);
static void cmd_export(pml::DB& db, const std::string& instr);
static void cmd_import(pml::DB& db, const std::string& instr);
static void cmd_search(pml::DB& db, const std::string& instr);
static bool parse_attrs(const std::string& instr, pml::Crypto::PwAttr& attrs, std::size_t& pwlen);

constexpr std::size_t get_total_pwlen(const pml::Crypto::PwAttr& attrs);

int main(int argc, char* argv[]) {

    // if command line options get more complicated, it would be worth using
    // a real parser

    if (argc > 1) {
        std::array<std::string, 3> cl_opts{"--help", "--gen=", "--easy"};
        std::string cmd(argv[1]);
        if (cmd.compare(0, cl_opts[0].size(), cl_opts[0]) == 0) {
            std::cerr << msg_usage << std::endl
                      << "In interactive mode:\n\n"
                      << msg_help << version_footer << pml::help::getversion() << std::endl;
            return 0;
        }
        else if (cmd.compare(0, cl_opts[1].size(), cl_opts[1]) == 0) {
            pml::DB empty_db;
            std::string genpw_cmd{cmd.substr(cl_opts[1].size())};
            if (genpw_cmd.size() > 0 && genpw_cmd.front() != 'g')
                genpw_cmd.insert(0, 1, 'g'); // we know pw gen command dont force user to repeat
            cmd_genpw(empty_db, genpw_cmd);
            return 0;
        }
        else if (cmd.compare(0, cl_opts[2].size(), cl_opts[2]) == 0) {
            pml::DB empty_db;
            std::string genpw_cmd{cmd.substr(cl_opts[2].size())};
            cmd_easygenpw(empty_db, genpw_cmd);
            return 0;
        }
    }

    const char* h;
    if (argc != 1 || (h=getenv("HOME")) == NULL) {
        std::cerr << msg_usage << version_footer << pml::help::getversion() << std::endl;
        return 1;
    }

    try {
        RevisionedFile dbfile(std::string(h) + "/.pwman", "db");

        pml::DB db;
        pml::Crypto crypto;
        PassPhrase pass;

        try {

            if (dbfile.exists()) {
                bool got_correct_passphrase = false;
                do {
                    pass.askUser("Enter current pass phrase");
                    try {
                        db = pml::DB::dbFromSerializedString(crypto.decrypt(pass.getPassPhrase(), dbfile.getCurrentVersion()));
                        got_correct_passphrase = true;
                    }
                    catch(std::exception& de) {
                        std::cerr << "could not decrypt passwords with pass phrase: "
                                  << de.what() << std::endl;
                    }
                }
                while (! got_correct_passphrase);
            }
            else
                pass.askUser("Enter pass phrase to use for your new password database");

            pm_repl(db);
        }
        catch (std::exception& e) {
            std::cerr << e.what() << std::endl;
        }

        std::cout << "database modified? " << db.modified() << std::endl;
        if (db.modified()) {
            try {
                dbfile.saveNewVersion(crypto.encrypt(pass.getPassPhrase(), pml::DB::serializedStringFromDb(db)));
                dbfile.commitChanges(); // needed???
            }
            catch(std::exception& de) {
                std::cerr << "could not save updated passwords: " << de.what() << std::endl;
            }
        }
    }
    catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}

void pm_repl(pml::DB& db) {
    std::string instr;

    for (bool keep_trying=true; keep_trying; ) {
        std::cout << "cmd: ";

        std::getline(std::cin, instr);

        if (std::cin.eof())
            throw pml::FatalException("user closed input");
        if (! instr.size())
            continue;

        switch(instr.front()) {
        case 'q':
            keep_trying = false;
            break;
        case 'g':
            cmd_genpw(db, instr);
            break;
        case 'G':
            cmd_easygenpw(db, instr);
            break;
        case 'a':
            cmd_addpw(db, instr);
            break;
        case 'e':
            cmd_export(db, instr);
            break;
        case 'i':
            cmd_import(db, instr);
            break;
        case 's':
            cmd_search(db, instr);
            break;
        case 'h':
        case '?':
            std::cout << msg_help << version_footer << pml::help::getversion() << std::endl;
            break;
        default:
            break;
        }
    }
}

void cmd_genpw(pml::DB&, const std::string& instr) {
    pml::Crypto c;
    pml::Crypto::PwAttr attrs;
    std::size_t pwlen = 0;

    if (parse_attrs(instr, attrs, pwlen)) {
        pml::secstring_t gen = c.genPassword(pwlen, attrs);
        std::cout << "Suggested password: " << gen << std::endl;
    }
    else {
        std::cout << "invalid password config: " << instr
                  << ", use : g<len>[lN][dN][sN]" << std::endl;
    }
}

// instr[0] could be
//   empty, G or =      - use default easy passwords attributes
//   Gnumber or =number - use number as total password length
void cmd_easygenpw(pml::DB&, const std::string& instr) {
    pml::Crypto c;
    pml::secstring_t gen;
    const std::string limited_special{".,#$"};

    if (instr.size() > 1 && (instr.front() == 'G' || instr.front() == '=')) {
        auto total_len = std::atoi(&instr[1]);
        if (total_len < 1) {
            std::cout << "invalid password length in easy gen - " << instr << std::endl;
            return;
        }

        pml::Crypto::PwAttr attrs{
            static_cast<int>(std::round(total_len * 0.78)),
            static_cast<int>(std::round(total_len * 0.11)),
            static_cast<int>(std::round(total_len * 0.11)),
            limited_special
        };
        std::size_t pwlen{get_total_pwlen(attrs)};
        gen = c.genPassword(pwlen, attrs);
    }
    else if (instr.size() < 2 || instr == "G" || instr == "=") {
        pml::Crypto::PwAttr attrs{14,2,2,limited_special};
        std::size_t pwlen{get_total_pwlen(attrs)};
        gen = c.genPassword(pwlen, attrs);
    }
    std::cout << "Suggested password: " << gen << std::endl;
}

void cmd_addpw(pml::DB& db, const std::string&) {
    std::string name, location, strbuf, strbuf1;

    std::cout << "Add new password entry" << std::endl;
    std::cout << "  entry name or short description: ";
    std::getline(std::cin, name);
    std::cout << "  URL or location: ";
    std::getline(std::cin, location);

    pml::Entry ent(name, location);

    std::cout << "  user login: ";
    std::getline(std::cin, strbuf);
    ent.addMetaData("user", strbuf);
    std::cout << "  password: ";
    std::getline(std::cin, strbuf);
    ent.addMetaData("pass", strbuf);

    std::cout << "  Add additional key/value pairs, extra data to store with password (CR to end)" << std::endl;
    do {
        std::cout << "    key: ";
        std::getline(std::cin, strbuf);
        if (strbuf.size() > 0) {
            std::cout << "    value: ";
            std::getline(std::cin, strbuf1);
            ent.addMetaData(strbuf, strbuf1);
        }
    } while (strbuf.size() > 0);

    db.addEntry(ent);
}

static std::tuple<std::string, std::string> parse_command(const std::string& s) {
    std::string cmd, arg;

    std::string::const_iterator w = std::find_if(s.begin(), s.end(), [](char c) {
            return isspace(c);
        });

    if (w != s.end()) {
        cmd = std::string(s.begin(), w);
        w = std::find_if(w, s.end(), [](char c) {
            return ! isspace(c);
        });
        if (w != s.end()) {
            arg = std::string(w, s.end());
        }
    }

    return std::make_tuple(cmd, arg);
}

void cmd_export(pml::DB& db, const std::string& instr) {
    auto rs = parse_command(instr);
    const std::string& filename(std::get<1>(rs));
    if (filename.size() == 0) {
        std::cout << pml::DB::serializedStringFromDb(db) << std::endl;
    }
    else {
        try {
            pml::IO io;
            std::cout << "writing export to [" << filename << "]" << std::endl;
            std::string expdata(pml::DB::serializedStringFromDb(db));
            io.writeFile(filename, pml::rawdata_t(expdata.begin(), expdata.end()));
        }
        catch (std::exception& e) {
            std::cerr << e.what() << std::endl;
        }
    }
}

void cmd_import(pml::DB& db, const std::string& instr) {
    auto rs = parse_command(instr);
    const std::string& filename(std::get<1>(rs));

    if (filename.size() == 0)
        return;

    pml::rawdata_t raw;
    try {
        pml::IO io;
        raw = io.readFile(filename);

        pml::secstring_t json(raw.begin(), raw.end());

        pml::DB tmp_db = pml::DB::dbFromSerializedString(json);
        std::vector<pml::Entry> add_ents = tmp_db.allRecords();
        for (auto newent : add_ents) {
            db.addEntry(newent);
        }
        std::cout << "imported/updated " << add_ents.size() << " entries" << std::endl;
    }
    catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
    }
    raw.assign(raw.size(), 0x20);  // erase contents from memory
}

void cmd_search(pml::DB& db, const std::string& instr) {
    int search_attrs = pml::DB::QUERY_BY_VALUE | pml::DB::QUERY_FLD_NAMLOC;
    auto p = ++instr.begin();

    while (p != instr.end() && *p != ' ') {
        switch (*p) {
        case 'a':   // query all fields
            search_attrs |= pml::DB::QUERY_FLD_REST;
            break;
        case 'f':   // query by key as well as value
            search_attrs |= pml::DB::QUERY_BY_KEY;
            break;
        case 'r':   // use regex
            search_attrs |= pml::DB::QUERY_TYPE_REGEX;
            break;
        case 'i':   // case insensitive
            search_attrs |= pml::DB::QUERY_TYPE_CINSENSITIVE;
            break;
        default:    // quietly skip unknown modifies
            break;
        }
        ++p;
    }
    while (p != instr.end() && *p == ' ')
        ++p;
    if (p == instr.end())
        return;

    std::vector<pml::Entry> finds = db.query(std::string(p, instr.end()), search_attrs);
    SearchResultsHandler(db, finds).handle();
}

bool parse_attrs(const std::string& instr, pml::Crypto::PwAttr& attrs, std::size_t& pwlen) {
    auto sp = instr.begin();

    ++sp; // move past 'g'

    if (! isdigit(*sp)) {
        std::cout << "must start with password length" << std::endl;
        return false;
    }
    else {
        while (sp != instr.end() && isdigit(*sp))
            pwlen = (*(sp++) - '0') + (pwlen * 10);
    }

    int* curtype = NULL;

    while (sp != instr.end()) {
        switch(*sp) {
        case 'l':
            curtype = &attrs.mixed_cnt;
            *curtype = 0;
            break;
        case 'd':
            curtype = &attrs.digit_cnt;
            *curtype = 0;
            break;
        case 's':
            curtype = &attrs.special_cnt;
            *curtype = 0;
            break;
        default:
            if (isdigit(*sp) && curtype != NULL)
                *curtype = (*sp - '0') + (*curtype * 10);
            break;
        }
        ++sp;
    }

    return true;
}

constexpr std::size_t get_total_pwlen(const pml::Crypto::PwAttr& attrs) {
    return attrs.mixed_cnt + attrs.digit_cnt + attrs.special_cnt;
}
