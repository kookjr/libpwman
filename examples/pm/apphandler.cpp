//    Copyright (C) 2016, 2017 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <cctype>

#include "apphandler.hpp"

SearchResultsHandler::SearchResultsHandler(pml::DB& db, std::vector<pml::Entry>& results) :
    CompletionHandler(db),
    mResults(results) {
    }

SearchResultsHandler::~SearchResultsHandler() {
}

void SearchResultsHandler::handle() {
    if (mResults.size() == 0)
        return;

    std::cout << "matches:" << std::endl;

    // print choices
    std::size_t i = 0;
    std::for_each(mResults.begin(), mResults.end(), [&i](pml::Entry& ep) {
            std::cout << "  " << i++ << ". "
                      << ep.getName() << ", "
                      << ep.getLocation() << std::endl;
        });

    // prompt and get one to print
    std::string in(queryUser("choose entry"));

    // XXX possibly option to just copy password to clipboard
    if (in.size()) {
        char cmd = 'd';

        if (! isdigit(in[0])) {
            cmd = in[0];
            in.erase(0, 1);
        }

        if (in.size() && isdigit(in[0])) {
            i = std::atoi(in.c_str());
            if (i < mResults.size()) {
                switch (cmd) {
                case 'd':  // display entry
                    std::cout << mResults[i];
                    break;
                case 'e':  // edit entry
                    EditEntryHandler(mDB, mResults[i]).handle();
                    break;
                case 'x':  // delet entry
                    std::cout << "deleting " << mResults[i].getName() << std::endl;
                    if (! mDB.deleteEntry(mResults[i])) {
                        std::cerr << "could not delete: " << mResults[i];
                    }
                    break;
                default:
                    break;
                }
            }
        }
    }
}


EditEntryHandler::EditEntryHandler(pml::DB& db, pml::Entry& entry) :
    CompletionHandler(db),
    mEntry(entry) {
    }

EditEntryHandler::~EditEntryHandler() {
}

void EditEntryHandler::handle() {
    std::string n, l;

    n = queryUser(std::string("name [" + mEntry.getName() + "]"), mEntry.getName());
    l = queryUser(std::string("location [" + mEntry.getLocation() + "]"), mEntry.getLocation());

    pml::Entry mod(n, l);

    std::vector<std::string> keys = mEntry.getMetaKeys();
    for (auto key : keys) {
        l = mEntry.getMetaValue(key);
        n = queryUser(std::string(key + " [" + l + "]"), l);
        mod.addMetaData(key, n);
    }

    std::string k, v;
    std::cout << "  Add additional key/value pairs, extra data to store with password (CR to end)" << std::endl;
    do {
        k = queryUser("   key");
        if (k.size() > 0) {
            v = queryUser(" value");
            mod.addMetaData(k, v);
        }
    } while (k.size() > 0);

    mDB.addEntry(mod);
}
