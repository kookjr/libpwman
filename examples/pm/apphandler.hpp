//    Copyright (C) 2016 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#ifndef APPHANDLERS_HPP
#define APPHANDLERS_HPP

#include <vector>

#include "completion.hpp"

class SearchResultsHandler : public CompletionHandler {
public:
    SearchResultsHandler(pml::DB& db, std::vector<pml::Entry>& results);
    virtual ~SearchResultsHandler();

    virtual void handle();

private:
    std::vector<pml::Entry>& mResults;
};



class EditEntryHandler : public CompletionHandler {
public:
    EditEntryHandler(pml::DB& db, pml::Entry& entry);
    virtual ~EditEntryHandler();

    virtual void handle();

private:
    pml::Entry& mEntry;
};

#endif  // APPHANDLERS_HPP
