//    Copyright (C) 2016 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#ifndef COMPLETION_HPP
#define COMPLETION_HPP

#include <string>

#include <pml/db.hpp>

class CompletionHandler {
public:
    CompletionHandler(pml::DB& db);
    virtual ~CompletionHandler();

    virtual void handle();

    std::string queryUser(const std::string& prompt);
    std::string queryUser(const std::string& prompt, const std::string& defvalue);

protected:
    pml::DB& mDB;
};

#endif  // COMPLETION_HPP
