//    Copyright (C) 2016 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>

#include <pml/helpers.hpp>

#include "passphrase.hpp"

void PassPhrase::askUser(const std::string& query) {
    unsigned flags = pml::help::USER_INPUT_NO_ECHO | pml::help::USER_INPUT_REJECT_EMPTY;
    mPassPhrase    = pml::help::askUser(query, flags);
}

const pml::secstring_t& PassPhrase::getPassPhrase() {
    return mPassPhrase;
}
