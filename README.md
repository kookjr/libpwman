[![pipeline status](https://gitlab.com/kookjr/libpwman/badges/master/pipeline.svg)](https://gitlab.com/kookjr/libpwman/commits/master)


# Password Manager Library

There are plenty of good encryption libraries out there but they tend to be feature rich, flexible and difficult to use safely. The purpose of this C++11 library is to create all the functions needed to write your own password manager with a simple interface that hides and encapsulates good security measures to manage and store an encrypted password data. The intent is to allow creative user interface developers to focus on a good application without being too concerned about the details of encryption.

Although the focus of this project is a library, it includes a fully functional command line password manager. It can be used as is or as an example of how to use the library.

The features provided are:
  * Import/export library using simple JSON format
  * Password generation that allows the user to customize character types and length
  * A simple in-memory database that provides; search, add, delete and update
  * Full featured searching
  * Encryption based on a single (hopefully long) pass phrase

> *IMPORTANT NOTE - This library has not undergone any official security audit. If it's used in any way that provides access to your password data, even in the encrypted from, you should have your own thorough security audit done. Contributions to the project are welcome.*

## Dependencies

This library has only a few dependencies
  * Modern C++ compiler featuring C++11 feature set
  * [libsodium](https://github.com/jedisct1/libsodium) encryption library
  * [jsoncpp](https://github.com/open-source-parsers/jsoncpp) JSON processing
  * [Catch](https://github.com/philsquared/Catch) unit test framework
  * cmake
  * python (at least version 2.6, for jsoncpp amalgamated source)
  * wget, autotools (get/build libsodium)
  * gcc

And a few more for the documentation (optional)
  * [PlantUML](http://plantuml.com/)
  * [GIMP](https://www.gimp.org/)

## License

GNU GENERAL PUBLIC LICENSE, Version 3. See COPYING file in source distribution.

# Encryption

Password data is encrypted with [libsodium](https://github.com/jedisct1/libsodium), currently version [1.0.20](https://github.com/jedisct1/libsodium/releases), using [authenticated encryption](https://download.libsodium.org/doc/secret-key_cryptography/authenticated_encryption.html). From the documentation this uses:
  * Encryption: XSalsa20 stream cipher
  * Authentication: Poly1305 MAC

All of the crypto is done in `lib/crypto.cpp`. The encrypted file has a small header defined by `struct encrypted_block` used as a first check to make sure the decrypted file is password data from this file before verification and decryption.

# Design

Although this is a library and can be used with any number of designs for a password manager, it was created while thinking of a particular application design. The goals for the application (included as an example) were:

  * Limit exposure to password data by only using a local database. Online support was intentionally avoided.
  * Create a portable interface that could be easily used to create at least a Linux and Android application.
  * Use exceptions out of band errors.

## Data Flow

With that in mind, here is the data flow through the application.

![Data Flow](docassets/genimages/dataflow.png)

While the application starts up, it read in the encrypted password data from local storage. Using a user supplied pass phrase the data is decrypted. The form at this point is the exported JSON format, basically an array of records, each have a set of key/value pairs. That data is then converted into the in-memory database which is the main interface for the application. If changes are made to the in-memory database, the process is reversed to save the encrypted data back to a file.

The sequence diagram below shows the classes that make up the library and the calls used for this data flow.

![Classes for Data Flow](docassets/genimages/dataflowseq.png)

## Components

The library is made up of three main components described below.

### IO

This class provides platform independent (currently POSIX/Linux) methods for file I/O and directory operations needed to store password data to disk. The methods provide functionality to:

  * check if a file or directory exists
  * make a directory
  * read and write a file in one operation
  * read a password, without echoing it, from the user (for command line apps)

### Crypto

The simplicity of this library is most evident in the Crypto class. There are only two encryption methods and one utility method to:

  * encrypte text data
  * decrypt encrypted data
  * generate random passwords based on criteria provided

The password generator allows the caller to configure what character types are included in the password, the amount of each and the total password length. The character types provided are:

  * Letters, upper and lower case
  * Digits, 0-9
  * Special, all other printable characters (with a few exceptions, see source code)

### DB

The database class provides the most functionality for the application. The database is made up of records of password data. Each record must have a name and location. This would usually be something like "Google" and "https://www.google.com/" but no formatting is imposed. The rest of the record is is made up of key/value pairs. The sample application enforces that two of these keys must be "user" and "password".

The class provides static methods to convert a database to and from JSON. Here is a sample of what a one entry database would look like.

```json
{
   "database" : [
      {
         "location" : "http://reddit.com/",
         "name" : "Reddit - the front page of the internet",
         "user" : "jeff",
         "pass" : "jeff_1"
      }
   ]
}
```

Once a database is loaded into memory it can be searched. The parameters of the search allow you to custom how and what is searched.

  * Search only field names (key), only field values or both
  * Search just the name and location fields, just the other fields or all fields
  * Do a case-insensitive search search (case-sensitive by default)

For editing, you can add, delete or modify existing records.

Searches returns records (Entry) which have their own interface for data access.

### Helpers

The library has an API to get its version string. It can be used by the application to display to the user, or verify incompatibility of a password database. It can be used like this:

```
#include <pml/helpers.hpp>
...
std::string libver = pml::help::getversion();
```

# Sample Application

A fully functional sample command line application is provided called `pm`. The first time you run it you will be asked for your password database master password. *NOTE: this password is NOT saved anywhere and the database is not recoverable without it.* Then it goes into interactive mode and will prompt you with the following prompt; `cmd:`

The available commands are displayed with the `help` command, and are as follows.

```
    Commands:
      quit
        q
      generate a random password
        g[<pwLen>][l<num>][d<num][>s<num>]
          pwLen - total length of password
          l<num> - num of letters in password
          d<num> - num of digits in password
          s<num> - num of special characters in password
        G[<pwLen>]
          pwLen - suggested total length of password
      add a new password entry
        a
      export password database, JSON format, to stdout or
      or to optional file_name
        e [file_name]
      import password database from JSON format
        i file_name
      search for a password entry
        s[afri] search_pattern
          a - query all fields, not just name and location
          f - search name of field too, not just value
          r - regular expression search (not supported)
          i - case insensitive search
          <see sub-commands below>
      help
        ? or h

    After search, these sub-commands:
      display or edit entry
        [d]<num> - display the numbered entry
        e<num>   - edit the numbered entry
        x<num>   - delete the numbered entry

    Version: 0.1.3
```

There are also two command line options available instead of going into command mode; one to generate a new password the other to display a help message. These features are sometimes useful without needing to access your password database. Here are some examples.

```
    pm --help            # display help/usage message
    pm --gen=g12         # generate a random 12 character password
    pm --gen=g14l8       # generate a random 14 character password with at most 8 letters
    pm --gen=g14l8d4s2   # generate a random 14 character password with 8 letters, 4 digits and 2 special
    pm --easy            # generate a random password that will work with most sites
    pm --easy=30         # generate a random 30 character password that will work with most sites
```

# Installation

Currently this library can be built on a Linux system. The library itself depends on a locally built libsodium archive library (libsodium.a). As the example program does, you'll have to compile and link with both libraries for your application (libsodium and libpmlib). The result will be a self contained executable not depending on system shared libraries for encryption.

## Updating to New Versions

If you are updating your version of Password Manager Library it's a good idea to backup your password file in a neutral format in case the new version introduces some incompatibility. If you are using `pm` use the following instructions. Otherwise use a similar procedure with your password manager.

Before updating, export your password database to a text file. In `pm` you can use the `e [file_name]`, like:
```
    e backup_stuff.txt
```

After you build and installed the new version of the library and your password manager:

  * Test the new version to make sure you can open your password file. If not continue to the next step
  * Remove the password database.
  * Start `pm`
  * Import your passwords with `i backup_stuff.txt`
  * Quit the program to save the new password database, `q`

Make sure to completely erase `backup_stuff.txt`, perhaps using a a command like:
```
    shred -u backup_stuff.txt
```

## Building from Source

[jsoncpp](https://github.com/open-source-parsers/jsoncpp) and [Catch](https://github.com/philsquared/Catch) are included as git submodules, and all necessary build procedures are included in libpwman's build process (below).

Note with the few noted changes below this package builds under the [Termux](https://termux.com/) Android app environment so the password manager sample application can be used on your phone.

Some Linux distrobution packages are needed. Last tested on Ubuntu 22.04 and Termux 0.118.0.

OS     | Packages
-------|------------
Ubuntu | build-essential git python wget
Termux | which build-essential git python wget

Clone this repository and get external packages.

```
  $ git clone https://gitlab.com/kookjr/libpwman
  $ cd libpwman
  $ git submodule init
  $ git submodule update
```

Next build libsodium (currently version 1.0.20) from a source tarball. A bash script is included to do this. You can refer to these [instructions](https://download.libsodium.org/doc/installation/index.html) if there are any issues. This only needs to be done one time after cloning the libpwman repository.

```
  $ cd pkgs
  $ ./get_libsodium.sh pkgroot
  # this will leave headers/libs for building in
  #   pkgs/sodium_work/pkgroot
  $ cd ..
```

Now build libpwman itself from the root of the libpwman repository.

```
  $ mkdir build && cd build && cmake .. && make
```

Note to make a debug build use the following cmake command:

```
  $ mkdir build-debug && cd build-debug && cmake -DCMAKE_BUILD_TYPE=Debug .. && make
```

You can install all of the components. This will allow you to use the example password manager and build against the library for other applications. The default install will be in `/usr/local`

```
  # from the build directory, type the following. Note you can install in a
  # different location by adding DESTDIR=/some/other/path between make and
  # install
  $ sudo make install
```

If you want to use the example password manager add `/usr/local/bin` to your PATH environment variable.

## Build Cleanup

To do the equivalent of a make clean

```
  rm -rf build
```

To do the equivalent of a make clobber

```
  rm -rf build pkgs/sodium_work
```

# Versions

This project uses the following versioning rules. Given a version in the form of A.B.C, the follow applies:
  * Change to A indicates an incompatible API change. B and C are reset to zero.
  * Change to B indicates a major functionality change or addition. C is reset to zero.
  * Change to C indicates a minor functionality change or addition.
  * Smaller changes and bug fixes will not change any part of the version, but will be tracked through repository commits.

Version | Description
--------|------------
v0.1.0 | Initial version
v0.1.1 | Submodule support for most external dependencies
v0.1.2 | Add delete entry option to example app
v0.1.3 | Build with loacal libsodium and CI
v0.2.0 | update libsodium to 1.0.16 from 1.0.10
v0.2.1 | Add easy password generation
v0.2.2 | Fix fetching of libsodium
v0.2.3 | update libsodium to 1.0.18 from 1.0.16
v0.2.4 | Fix CI build on GL
v0.2.5 | Update build to latest base OS, libsodium to 1.0.20
