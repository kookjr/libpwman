#ifndef PML_TYPES_HPP
#define PML_TYPES_HPP

//    Copyright (C) 2016 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <vector>
#include <stdexcept>
#include <string>

#define STRINGIFY2(str) #str
#define STRINGIFY(str)  STRINGIFY2(str)

namespace pml {

    // Exceptions

    class GeneralException : public std::runtime_error {
    public:
        GeneralException() :
            std::runtime_error("exception")
            { }
        GeneralException(const char* what) :
            std::runtime_error(std::string("exception: ") + what)
            { }
        GeneralException(const std::string what) :
            std::runtime_error(std::string("exception: ") + what)
            {}
    };

    class SerializeException : public std::runtime_error {
    public:
        SerializeException() :
            std::runtime_error("serialization")
            { }
        SerializeException(const char* what) :
            std::runtime_error(std::string("serialization: ") + what)
            { }
        SerializeException(const std::string what) :
            std::runtime_error(std::string("serialization: ") + what)
            {}
    };

    class IOException : public std::runtime_error {
    public:
        IOException() :
            std::runtime_error("input/output")
            { }
        IOException(const char* what) :
            std::runtime_error(std::string("input/output: ") + what)
            { }
        IOException(const std::string what) :
            std::runtime_error(std::string("input/output: ") + what)
            {}
    };

    class FatalException : public std::runtime_error {  // meant to be fatal
    public:
        FatalException() :
            std::runtime_error("fatal")
            { }
        FatalException(const char* what) :
            std::runtime_error(std::string("fatal: ") + what)
            { }
        FatalException(const std::string what) :
            std::runtime_error(std::string("fatal: ") + what)
            {}
    };

    // binary data container
    using encdata_t = std::vector<unsigned char>;
    using rawdata_t = std::vector<unsigned char>;

    // safe container for plain text secret data - impl TBD
    using secstring_t = std::string;
}

#endif // PML_TYPES_HPP
