#ifndef PML_HELPERS_HPP
#define PML_HELPERS_HPP

//    Copyright (C) 2016 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <string>
#include <vector>

namespace pml {
    namespace help {
        // return the version of libpwman
        std::string getversion();

        // honor each delim; "1  2" -> "1" "" "2"
        std::vector<std::string> tokens (const std::string& str, char delim);
        // combine delims;   "1  2" -> "1" "2"
        std::vector<std::string> mtokens(const std::string& str, char delim);

        // for flags in askUser(), bit fields to OR together
        const unsigned USER_INPUT_NO_ECHO      = 0x01;
        const unsigned USER_INPUT_REJECT_EMPTY = 0x02;
        std::string askUser(const std::string& prompt, unsigned flags=0U);
    }
}

#endif // PML_HELPERS_HPP
