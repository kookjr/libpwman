#ifndef PML_CRYPTO_HPP
#define PML_CRYPTO_HPP

//    Copyright (C) 2016, 2021 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <string>
#include <pml/types.hpp>

namespace pml {
    class Crypto {
    public:
        struct PwAttr {
            /*
             * For each field specify the number of characters of this
             * type that should be in the password. Supported values
             * are:
             *   N < 0 --> Random number of these
             *   N = 0 --> None of this type
             *   N > 0 --> Exactly this many (if password length supports)
             * An attempt is made to have an even distribution of N < 0 for
             * all types.
             */
            int mixed_cnt;
            int digit_cnt;
            int special_cnt;

            // allow an alternate special character set to be passed in
            std::string alt_special;

            PwAttr() : mixed_cnt(-1), digit_cnt(-1), special_cnt(-1), alt_special() {}
            PwAttr(int m, int d, int s) : mixed_cnt(m), digit_cnt(d), special_cnt(s), alt_special() {}
            PwAttr(int m, int d, int s, const std::string& sp) : mixed_cnt(m), digit_cnt(d), special_cnt(s), alt_special(sp) {}
        };

    public:
        Crypto();
        ~Crypto() = default;

        encdata_t   encrypt(const secstring_t& pass, const secstring_t& plain_text)  const;
        secstring_t decrypt(const secstring_t& pass, const encdata_t&   cypher_text) const;

        secstring_t genPassword(std::size_t len, const PwAttr& attrs) const;
    };
}

#endif // PML_CRYPTO_HPP
