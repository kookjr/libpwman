#ifndef PML_PREFS_HPP
#define PML_PREFS_HPP

//    Copyright (C) 2016 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

namespace pml {
    class Preferences {
    public:
        enum class keyType {
            PREF_PW_LOCATION
        };

        Preferences(std::string prefdata);
        ~Preferences();

        std::string lookupPref(keyType key) const;
    private:
    };
}

#endif // PML_PREFS_HPP
