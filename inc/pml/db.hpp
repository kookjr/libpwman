#ifndef PML_DB_HPP
#define PML_DB_HPP

//    Copyright (C) 2016 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <vector>
#include <map>
#include <unordered_set>
#include <algorithm>

namespace pml {
    // Notes:
    //   - name/location make up key of entry
    //   - addMetaData overwrites existing entry or adds new
    class Entry {
    public:
        Entry();
        Entry(const std::string& name, const std::string& location);

        ~Entry();

        void addMetaData(const std::string& key, const std::string& val);
        bool hasKey(const std::string& key) const;
        bool hasValue(const std::string& key) const;
        std::vector<std::string> getMetaKeys() const;

        bool operator==(const Entry& rhs) const;
        bool operator< (const Entry& rhs) const;

        std::string getName() const;
        std::string getLocation() const;
        std::string getMetaValue(const std::string& key) const;

    private:
        typedef std::map<std::string,std::string> EntMap;

        EntMap mKV;
        std::string mName;
        std::string mLocation;
    };

    std::ostream& operator<<(std::ostream& o, const Entry& e);
}

namespace std {
    template <>
    struct hash<pml::Entry> {
        size_t operator()(const pml::Entry& e) const {
            return ((hash<string>()(e.getName()) ^ (hash<string>()(e.getLocation()) << 1)) >> 1);
        }
    };
}

namespace pml {
    // database access - in memory
    class DB {
    public:
        DB();
        ~DB();

        enum {
            // what part of field
            QUERY_BY_KEY            = 0x0001,
            QUERY_BY_VALUE          = 0x0002,
            // what fields
            QUERY_FLD_NAMLOC        = 0x0004,
            QUERY_FLD_REST          = 0x0008,
            // how to query
            QUERY_TYPE_REGEX        = 0x0010,
            QUERY_TYPE_CINSENSITIVE = 0x0020
        };

        static DB dbFromSerializedString(const std::string& dbstr);
        static std::string serializedStringFromDb(const DB& db);

        DB& addEntry(const Entry& ent);
        bool deleteEntry(const Entry& ent);

        bool findEntry(const Entry& record, Entry& result) const;
        std::vector<Entry> query(const std::string& str, int what=(QUERY_FLD_NAMLOC|QUERY_BY_VALUE)) const;

        std::vector<Entry> allRecords() const;

        bool modified() const;
        void resetModified();

    private:
        bool submatch(const std::string& pat, const std::string& str, bool cinsen, bool regex) const;
        std::unordered_set<Entry> mEntries;
        bool mModified;
    };
}

#endif // PML_DB_HPP
