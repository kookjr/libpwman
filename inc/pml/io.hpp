#ifndef PML_IO_HPP
#define PML_IO_HPP

//    Copyright (C) 2016 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <string>

#include <pml/types.hpp>

namespace pml {
    class IO {
    public:
        IO();
        ~IO();

        bool dirExists(const std::string& dir_path) const;
        bool fileExists(const std::string& file_path) const;

        void makeDirectory(const std::string& dir_path) const;

        rawdata_t readFile(const std::string& file_path) const;
        void writeFile(const std::string& file_path, const rawdata_t& contents) const;

        std::string readPassword(const std::string prompt);

    private:
    };
}

#endif // PML_IO_HPP
