#!/bin/bash

#    Copyright (C) 2021, 2023, 2024 Mathew Cucuzella, kookjr@gmail.com
#
#    This file is part of Password Manager Library; a program to store
#    passwords.
#
#    Password Manager Library is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Password Manager Library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

# Usage: $0 [relative-install-dir]
#
#    Ex: getlibsodium.sh install_root

function die()
{
    echo "Error: " $*
    exit 1
}

mkdir sodium_work || \
    die "Could not make working build dir. Remove if one exists"

pushd sodium_work >/dev/null

SODIUMVER=1.0.20
SODIUMTAR=libsodium-${SODIUMVER}.tar.gz
SODIUMSRC=$(pwd)/libsodium-${SODIUMVER}
SODIUMCFG=
INS=

if [ $# -eq 1 ]
then
    echo "Setting local libsodium INSTALL dir: $1"
    INS=$(pwd)/$1
    SODIUMCFG="--prefix=${INS}"

    mkdir ${INS} || \
        die "Could not make install root: $(INS)"
fi

if [ ! -d libsodium-${SODIUMVER} ]
then
    ######################  download libsodium ######################
    which wget >/dev/null 2>&1 || \
        die "Need wget to download libsodium"
    echo "Downloading libsodium..."
    wget --quiet https://download.libsodium.org/libsodium/releases/${SODIUMTAR} || \
        die "Could not download libsodium with wget"
    tar xzf ${SODIUMTAR} || \
        die "Could not extract libsodium"
    test -d libsodium-${SODIUMVER} || \
        die "Where is the extracted libsodium?"

    ######################  build it  ######################
    pushd libsodium-${SODIUMVER} >/dev/null

    # not sure why configure does not find these correctl,
    # maybe termux setup is unusual:
    #  ld - not found
    #  ar - not found
    which ld >/dev/null 2>&1
    if [ $? -nq 0 ]
    then
        export LD=ld.lld
    fi
    which ar >/dev/null 2>&1
    if [ $? -nq 0 ]
    then
        export AR=llvm-ar
    fi

    echo "Building libsodium"
    ./configure ${SODIUMCFG} || \
        die "Could not configure libsodium"
    make || \
        die "Could not make libsodium"
    make check || \
        die "Check failed on libsodium!"
    if [ -z "${INS}" ]
    then
        # let use do system install (needs sudo)
        echo -e "\n!!------------------------------------------------------------!!"
        echo    "!!------------------------------------------------------------!!"
        echo    "To complete setup for libpwman, install libsodium with:"
        echo    "    sudo make install"
        echo    "from ${SODIUMSRC}"
    else
        # automaticall do local install
        make install || \
            die "Could not install local copy of libsodium!"
    fi

    popd >/dev/null  # libsodium dir
    ######################  done building it  ######################
fi

popd >/dev/null  # sodium_work

exit 0
