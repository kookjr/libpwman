//    Copyright (C) 2016 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <string>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <iterator>

#include <string.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <pml/types.hpp>
#include <pml/io.hpp>

namespace pml {
    IO::IO() {
    }

    IO::~IO() {
    }

    rawdata_t IO::readFile(const std::string& file_path) const {
        try {
            std::ifstream is;
            is.exceptions(std::ifstream::badbit|std::ifstream::failbit);
            is.open(file_path, std::ios::binary|std::ios_base::in);

            // calculate necessary space
            is.seekg(0, std::ios::end);   
            std::streampos pos = is.tellg();
            is.seekg(0, std::ios::beg);

            // pre-allocate and fill buffer to return
            rawdata_t file_contents;
            file_contents.resize(pos);
            is.read((char* )&file_contents[0], pos);

            return file_contents;
        }
        catch (std::exception& e) {
            throw IOException(e.what());
        }
    }

    void IO::writeFile(const std::string& file_path, const rawdata_t& contents) const {
        try {
            std::ofstream os(file_path, std::ios::binary|std::ios_base::trunc|std::ios_base::out);
            os.exceptions(std::ofstream::failbit|std::ofstream::badbit);
            std::copy(contents.begin(), contents.end(),
                      std::ostream_iterator<char>(os));
        }
        catch (std::exception& e) {
            throw IOException(e.what());
        }
    }

    std::string IO::readPassword(const std::string prompt) {
        struct termios input;

        if (tcgetattr(STDIN_FILENO, &input) != 0)
            throw IOException("could not hide typing");
        input.c_lflag &= ~ECHO;
        if (tcsetattr(STDIN_FILENO, TCSANOW, &input) != 0)
            throw IOException("could not hide typing");

        std::string pwstr;
        std::cout << prompt;
        std::getline(std::cin, pwstr);

        input.c_lflag |= ECHO;
        int r = tcsetattr(STDIN_FILENO, TCSANOW, &input);

        if (std::cin.eof())
            throw FatalException("user closed input");
        else if (r != 0)
            throw IOException("could not unhide typing");

        return pwstr;
    }

    bool IO::dirExists(const std::string& dir_path) const {
        struct stat buf;
        return stat(dir_path.c_str(), &buf) == 0 && S_ISDIR(buf.st_mode);
    }

    bool IO::fileExists(const std::string& file_path) const {
        struct stat buf;
        return stat(file_path.c_str(), &buf) == 0 && S_ISREG(buf.st_mode);
    }

    void IO::makeDirectory(const std::string& dir_path) const {
        if (mkdir(dir_path.c_str(), 0700) != 0) {
            auto saverr = errno;
            char const* prefix;

            switch (saverr) {
            case ENOENT:
            case ENOTDIR:
                prefix = "trying to create database dir in non-existent directory";
                break;
            case EPERM:
                prefix = "you do not have permission to create database directory";
                break;
            default:
                prefix = "unable to make database directory";
                break;
            }

            std::string msg(prefix);
            msg.append(": ");
            msg.append(dir_path);
            msg.append(" - ");
            msg.append(strerror(saverr));

            throw IOException(msg);
        }
    }
}
