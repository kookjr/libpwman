//    Copyright (C) 2016, 2019, 2021-2022 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <vector>
#include <memory>

#include <sodium.h>

#include <pml/crypto.hpp>

// when allocating, encrypted_block memory, MUST be
//   sizeof(encrypted_block)
//   + CYPHER_TEXT_HEADER_SIZE
//   + actual text lenght
#define CYPHER_TEXT_HEADER_SIZE  crypto_secretbox_MACBYTES
#define calc_ct_size(s)          (s + CYPHER_TEXT_HEADER_SIZE)

#define ENCRYPTED_BLOCK_VERSION  1

namespace pml {

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
    struct encrypted_block {
        unsigned char version;
        unsigned char magicpad[3];
        unsigned char salt[crypto_pwhash_SALTBYTES];
        unsigned char nonce[crypto_secretbox_NONCEBYTES];
        unsigned int  plain_text_len;
        unsigned char cypher_text[0];
    };
#pragma GCC diagnostic pop

    struct SodiumFreetor {
        void operator()(encrypted_block* b) {
            sodium_free((void* )b);
        }
    };

    static const std::vector<std::vector<char> > all_choices {
        {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
                'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
                'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                'Y', 'Z'},
        {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'},
        {'@', '#', '$', '_', '&', '-', '+', '(', ')', '/',
                '*', '"', '\'', ':', ';', '!', '?'}
    };

    static void adjustCounts(int (&cnts)[3], std::size_t max, std::size_t so_far) {
        std::size_t to_go    = max - so_far;
        std::size_t required =
            (cnts[0] > 0 ? cnts[0] : 0) +
            (cnts[1] > 0 ? cnts[1] : 0) +
            (cnts[2] > 0 ? cnts[2] : 0) ;

        if (required == to_go) {
            if (cnts[0] == -1) cnts[0] = 0;
            if (cnts[1] == -1) cnts[1] = 0;
            if (cnts[2] == -1) cnts[2] = 0;
        }
    }

    Crypto::Crypto() {
        if (sodium_init() == -1)
            throw GeneralException("could not initialize encryption engine");
    }

    encdata_t Crypto::encrypt(const secstring_t& pass, const secstring_t& plain_text) const {
        // allocate block for eventual return
        std::size_t full_size = sizeof(encrypted_block) + calc_ct_size(plain_text.size());
        std::unique_ptr<encrypted_block, SodiumFreetor> block(static_cast<encrypted_block*>(sodium_malloc(full_size)));

        // setup known fields
        block->version = ENCRYPTED_BLOCK_VERSION;
        block->plain_text_len = plain_text.size();
        block->magicpad[0] = 0xBE;
        block->magicpad[1] = 0xEF;
        block->magicpad[2] = 0x14;

        // setup salt for this password hashing
        randombytes_buf(block->salt, sizeof(block->salt));

        // setup nonce for this encryption/decryption
        randombytes_buf(block->nonce, sizeof(block->nonce));

        // generate key from pass phrase and nonce
        unsigned char key[crypto_secretbox_KEYBYTES];
        int r = crypto_pwhash(key, (unsigned long long )sizeof(key),
                              pass.data(), pass.size(),
                              block->salt,
                              crypto_pwhash_OPSLIMIT_INTERACTIVE, crypto_pwhash_MEMLIMIT_INTERACTIVE,
                              crypto_pwhash_ALG_DEFAULT);
        if (r != 0)
            throw GeneralException("password hashing failed");

        crypto_secretbox_easy(block->cypher_text,
                              (const unsigned char* )plain_text.data(),
                              (unsigned long long   )plain_text.size(),
                              block->nonce,
                              key);

        return encdata_t( (unsigned char* )block.get(),
                          ((unsigned char* )block.get()) + full_size );
    }

    secstring_t Crypto::decrypt(const secstring_t& pass, const encdata_t& cypher_text) const {
        // cypher text MUST be an encrypted_block, so a few checks
        const encrypted_block * block = reinterpret_cast<const encrypted_block*>(cypher_text.data());
        if (cypher_text.size() < sizeof(encrypted_block)
            || block->version != ENCRYPTED_BLOCK_VERSION
            || block->magicpad[0] != 0xBE
            || block->magicpad[1] != 0xEF
            || block->magicpad[2] != 0x14)
            throw GeneralException("invalid cypher_text format");

        // generate key from pass phrase and nonce
        unsigned char key[crypto_secretbox_KEYBYTES];
        int r = crypto_pwhash(key, (unsigned long long )sizeof(key),
                              pass.data(), pass.size(),
                              block->salt,
                              crypto_pwhash_OPSLIMIT_INTERACTIVE, crypto_pwhash_MEMLIMIT_INTERACTIVE,
                              crypto_pwhash_ALG_DEFAULT);
        if (r != 0)
            throw GeneralException("password hashing failed");

        // decrypt - use vector to manage memory
        std::vector<unsigned char> locbuf(block->plain_text_len);
        auto new_plain_text = locbuf.data();
        r = crypto_secretbox_open_easy(new_plain_text,
                                       block->cypher_text,
                                       calc_ct_size(block->plain_text_len),
                                       block->nonce,
                                       key);
        if (r != 0)
            throw GeneralException("decryption failed");

        return secstring_t((char* )&new_plain_text[0], (char* )&new_plain_text[locbuf.size()]);
    }

    secstring_t Crypto::genPassword(std::size_t len, const PwAttr& attrs) const {
        secstring_t pass;

        int cnts[] = {attrs.mixed_cnt, attrs.digit_cnt, attrs.special_cnt};

        // make a local copy and modify if reqeusted
        auto local_all_choices = all_choices;
        if (!attrs.alt_special.empty()) {
            std::vector<char> new_specials(attrs.alt_special.begin(), attrs.alt_special.end());
            local_all_choices[2] = std::move(new_specials);
        }

        // attrs cannot make limits that would be impossible to
        // generate a password of len, in that case allow unlimited
        // number of letters
        if ( (cnts[0] != -1 && cnts[1] != -1 && cnts[2] != -1) &&
             ((uint32_t )(cnts[0] + cnts[1] + cnts[2])) < len )
            cnts[0] = -1;

        while (pass.size() < len) {
            // use a uniform random number to select the different
            // types of characters so we roughly get the same number
            // of each
            uint32_t rnd_char_set_idx = randombytes_uniform(local_all_choices.size());

            // check to see if we need any more characters of this type
            if (cnts[rnd_char_set_idx] != 0) {
                // adjust -1 settings if we may not get the exact
                // counts we need
                adjustCounts(cnts, len, pass.size());

                // use a "random" random number to select a character
                // from the set, there is no reason to avoid
                // duplicates
                const std::vector<char>& cset = local_all_choices[rnd_char_set_idx];
                pass.append(1, cset[randombytes_random() % cset.size()]);

                // update counts left
                if (cnts[rnd_char_set_idx] > 0) {
                    --cnts[rnd_char_set_idx];
                }
            }
        }

        return pass;
    }
}
