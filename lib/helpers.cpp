//    Copyright (C) 2016 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <sstream>

#include <termios.h>
#include <unistd.h>

#include <pml/types.hpp>
#include <pml/helpers.hpp>

namespace pml {
    namespace help {
        std::string getversion() {
            return STRINGIFY(PMLIB_VERSION);
        }

        static void echoTyping(bool echo) {
            struct termios tty;

            if (tcgetattr(STDIN_FILENO, &tty) != 0)
                throw pml::FatalException("no TTY attributes for standard input");

            if (echo)
                tty.c_lflag |= ECHO;
            else
                tty.c_lflag &= ~ECHO;

            if (tcsetattr(STDIN_FILENO, TCSANOW, &tty) != 0)
                throw pml::FatalException("could not hide password typing");
        }

        static std::vector<std::string> tokens(const std::string& str, char delim, bool allow_empty_tokens) {
            std::stringstream ss(str);
            std::vector<std::string> toks;
            std::string item;

            while (std::getline(ss, item, delim)) {
                if (item.empty() && ! allow_empty_tokens)
                    continue;
                toks.push_back(item);
            }

            return toks;
        }
        std::vector<std::string> tokens (const std::string& str, char delim) {
            return tokens(str, delim, true);
        }

        std::vector<std::string> mtokens(const std::string& str, char delim) {
            return tokens(str, delim, false);
        }

        std::string askUser(const std::string& prompt, unsigned flags) {
            std::string rstr;
            bool keep_asking = flags & USER_INPUT_REJECT_EMPTY;

            do {
                std::cout << prompt << ": ";

                if (flags & USER_INPUT_NO_ECHO)
                    echoTyping(false);

                std::getline(std::cin, rstr);

                if (flags & USER_INPUT_NO_ECHO) {
                    echoTyping(true);
                    std::cout << std::endl;
                }

                if (std::cin.eof())
                    throw pml::FatalException("user closed input");

                if (keep_asking && rstr.size() > 0)
                    keep_asking = false;
            }
            while (keep_asking);

            return rstr;
        }
    }
}
