//    Copyright (C) 2016 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <sstream>
#include <algorithm>

#include <json/json.h>

#include <pml/types.hpp>
#include <pml/db.hpp>

#define KEY_NAME "name"
#define KEY_LOCA "location"

namespace pml {
    DB::DB() :
        mEntries(),
        mModified(false) {
    }

    DB::~DB() {
    }

    DB& DB::addEntry(const Entry& ent) {
        if (ent.getName().size() == 0 || ent.getLocation().size() ==0) {
            /*
            if (ent.getName().size() != 0)
                std::cout << ent.getName() << std::endl;
            if (ent.getLocation().size() != 0)
                std::cout << ent.getLocation() << std::endl;
            */
            throw GeneralException("invalid record - empty key(s)");
        }

        // erase if already contained
        mEntries.erase(ent);
        // add new one
        mEntries.insert(ent);

        mModified = true;

        return *this;
    }

    bool DB::deleteEntry(const Entry& ent) {
        bool r = (mEntries.erase(ent) == 1);

        if (r)
            mModified = true;

        return r;
    }

    bool DB::findEntry(const Entry& record, Entry& result) const {
        bool r = false;
        std::unordered_set<Entry>::const_iterator it = mEntries.find(record);

        if (it != mEntries.end()) {
            result = *it;
            r = true;
        }

        return r;
    }

    std::vector<Entry> DB::query(const std::string& str, int what) const {
        std::vector<Entry> ents;

        bool regex = what & QUERY_TYPE_REGEX;
        bool cinse = what & QUERY_TYPE_CINSENSITIVE;

        for (auto it : mEntries) {
            if (what & QUERY_FLD_NAMLOC) {
                if (what & QUERY_BY_VALUE) {
                    if (submatch(str, it.getName(), cinse, regex) ||
                        submatch(str, it.getLocation(), cinse, regex)) {
                        ents.push_back(it);
                        continue;
                    }
                }
                if (what & QUERY_BY_KEY) {
                    if (submatch(str, KEY_NAME, cinse, regex) ||
                        submatch(str, KEY_LOCA, cinse, regex)) {
                        ents.push_back(it);
                        continue;
                    }
                }
            }
            if (what & QUERY_FLD_REST) {
                std::vector<std::string> keys = it.getMetaKeys();
                for (auto key : keys) {
                    if (what & QUERY_BY_VALUE) {
                        if (submatch(str, it.getMetaValue(key), cinse, regex)) {
                            ents.push_back(it);
                            break;
                        }
                    }
                    if (what & QUERY_BY_KEY) {
                        if (submatch(str, key, cinse, regex)) {
                            ents.push_back(it);
                            break;
                        }
                    }
                }
            }
        }

        return ents;
    }

    std::vector<Entry> DB::allRecords() const {
        std::vector<Entry> r;
        std::copy(std::begin(mEntries), std::end(mEntries), std::back_inserter(r));
        return r;
    }

    // serialization

    DB DB::dbFromSerializedString(const std::string& dbstr) {
        std::istringstream iss(dbstr);
        std::string errs;

        Json::CharReaderBuilder reader;
        Json::Value root;

        try {
            // parse inut
            if (! Json::parseFromStream(reader, iss, &root, &errs)) {
                throw SerializeException(errs);
            }
        }
        catch (SerializeException& e) {
            throw;
        }
        catch (std::exception& e) {
            throw SerializeException(e.what());
        }

        DB db;
        Json::Value ent_array;

        ent_array = root["database"];

        if (ent_array.isNull())
            throw SerializeException("no database entry");
        if (! ent_array.isArray())
            throw SerializeException("database is not an arry of entries");

        for (auto it : ent_array) {
            if (! it.isObject() ||
                it[KEY_NAME].isNull() ||
                it[KEY_LOCA].isNull() ) {
                throw SerializeException("entry missing name/location");
            }

            Entry ent(it[KEY_NAME].asString(), it[KEY_LOCA].asString());
            Json::Value::Members keys = it.getMemberNames();
            for (auto key : keys) {
                if (key == KEY_NAME || key == KEY_LOCA)
                    continue;
                ent.addMetaData(key, it[key].asString());
            }

            db.addEntry(ent);
        }

        db.resetModified();

        return db;
    }

    std::string DB::serializedStringFromDb(const DB& db) {
        Json::Value root;
        Json::Value entarray(Json::arrayValue);

        std::vector<Entry> ents = db.allRecords();
        for (auto ent : ents) {
            Json::Value v(Json::objectValue);

            v[KEY_NAME] = ent.getName();
            v[KEY_LOCA] = ent.getLocation();

            std::vector<std::string> meta_keys = ent.getMetaKeys();

            for (auto key : meta_keys) {
                v[key] = ent.getMetaValue(key);
            }

            entarray.append(v);
        }

        root["database"] = entarray;

        return root.toStyledString();
    }

    bool DB::modified() const {
        return mModified;
    }

    void DB::resetModified() {
        mModified = false;
    }

    bool DB::submatch(const std::string& pat, const std::string& str, bool cinsen, bool regex) const {
        bool r = false;
        if (regex) {
            // not supported
        }
        else if (cinsen) {
            r = (std::search(str.begin(), str.end(), pat.begin(), pat.end(),
                             [](char c1, char c2) { return std::toupper(c1) == std::toupper(c2); })
                != str.end());
        }
        else {
            r = (std::search(str.begin(), str.end(), pat.begin(), pat.end()) != str.end());
        }
        return r;
    }

    // entry

    Entry::Entry() :
        mKV(),
        mName(),
        mLocation() {
    }

    Entry::Entry(const std::string& name, const std::string& location) :
        mKV(),
        mName(name),
        mLocation(location) {
    }

    Entry::~Entry() {
    }

    void Entry::addMetaData(const std::string& key, const std::string& val) {
        mKV[key] = val;
    }

    bool Entry::hasKey(const std::string& key) const {
        return mKV.find(key) != mKV.end();
    }

    bool Entry::hasValue(const std::string& key) const {
        return std::any_of(std::begin(mKV), std::end(mKV),
                           [&key](const std::pair<std::string,std::string>& val) {
                               // may want to substring search
                               return val.second == key;
                           });
    }

    bool Entry::operator==(const Entry& rhs) const {
        return rhs.mName == mName && rhs.mLocation == mLocation;
    }

    bool Entry::operator<(const Entry& rhs) const {
        return (mName != rhs.mName) ? mName < rhs.mName : mLocation < rhs.mLocation;
    }

    std::string Entry::getName() const {
        return mName;
    }

    std::string Entry::getLocation() const {
        return mLocation;
    }

    std::vector<std::string> Entry::getMetaKeys() const {
        std::vector<std::string> r;

        std::for_each(std::begin(mKV), std::end(mKV),
                      [&r](const std::pair<std::string,std::string>& val) {
                          r.push_back(val.first);
                      });

        return r;
    }

    std::string Entry::getMetaValue(const std::string& key) const {
        auto it = mKV.find(key);

        if (it == mKV.end())
            throw SerializeException(std::string("key not in map: ") + key);

        return it->second;
    }

    std::ostream& operator<<(std::ostream& o, const Entry& e){
        o << e.getName() << ", " << e.getLocation() << std::endl;

        auto v = e.getMetaKeys();

        for_each(v.begin(), v.end(), [&](std::string& key) {
                o << "  " << key << "=" << e.getMetaValue(key) << std::endl;
            });

        return o;
    }

}
