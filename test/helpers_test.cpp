//    Copyright (C) 2016 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include "catch.hpp"
#include <pml/helpers.hpp>

static const char sp_delim = ' ';

TEST_CASE("easy parse") {
    std::string tokstr("one two three");
    std::vector<std::string> toks;

    SECTION("tokens") {
        toks = pml::help::tokens(tokstr, sp_delim);
    }

    SECTION("combine multiple tokens") {
        toks = pml::help::mtokens(tokstr, sp_delim);
    }

    REQUIRE(toks.size() == 3U);
    REQUIRE(toks[0] == "one");
    REQUIRE(toks[1] == "two");
    REQUIRE(toks[2] == "three");
}

TEST_CASE("back-to-back delims") {
    std::string tokstr("one  two");

    SECTION("tokens") {
        std::vector<std::string> toks(pml::help::tokens(tokstr, ' '));

        REQUIRE(toks.size() == 3U);
        REQUIRE(toks[0] == "one");
        REQUIRE(toks[1] == "");
        REQUIRE(toks[2] == "two");
    }

    SECTION("combine multiple tokens") {
        std::vector<std::string> toks(pml::help::mtokens(tokstr, ' '));

        REQUIRE(toks.size() == 2U);
        REQUIRE(toks[0] == "one");
        REQUIRE(toks[1] == "two");
    }
}

TEST_CASE("leading and trailing delims") {
    std::string tokstr(" one two ");

    SECTION("tokens") {
        std::vector<std::string> toks(pml::help::tokens(tokstr, ' '));

        REQUIRE(toks.size() == 3U);
        REQUIRE(toks[0] == "");
        REQUIRE(toks[1] == "one");
        REQUIRE(toks[2] == "two");
    }

    SECTION("combine multiple tokens") {
        std::vector<std::string> toks(pml::help::mtokens(tokstr, ' '));

        REQUIRE(toks.size() == 2U);
        REQUIRE(toks[0] == "one");
        REQUIRE(toks[1] == "two");
    }
}
