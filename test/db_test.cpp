//    Copyright (C) 2016, 2022 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include "catch.hpp"
#include <pml/types.hpp>
#include <pml/db.hpp>

//
//  Database Tests
//

TEST_CASE("adding entries") {
    pml::DB db;
    std::vector<pml::Entry> results;

    SECTION("empty check") {
        results = db.allRecords();
        REQUIRE(results.size() == 0U);
    }

    SECTION("add 1 record") {
        pml::Entry ent1("bank1", "http://example.com/");
        db.addEntry(ent1);

        results = db.allRecords();
        REQUIRE(results.size() == 1U);
    }

    SECTION("add duplicate record") {
        pml::Entry ent1("bank1", "http://example.com/");
        db.addEntry(ent1);
        db.addEntry(ent1);

        results = db.allRecords();
        REQUIRE(results.size() == 1U);
    }

    SECTION("add 2 records") {
        pml::Entry ent1("bank1", "http://example.com/");
        pml::Entry ent2("bank2", "http://example.com/");
        db.addEntry(ent1)
            .addEntry(ent2);

        results = db.allRecords();
        REQUIRE(results.size() == 2U);
    }

    SECTION("add invalid record") {
        pml::Entry ent1;
        // expect_exception(db.addEntry(ent1));
    }
}

TEST_CASE("checking all entries") {
    pml::DB db;

    pml::Entry ent1("bank1", "http://example.com/");
    pml::Entry ent2("bank2", "http://example.com/");
    db.addEntry(ent1)
        .addEntry(ent2);

    REQUIRE(db.allRecords().size() == 2U);
}

TEST_CASE("deleting entries") {
    pml::DB db;

    pml::Entry ent1("bank1", "http://example.com/");
    pml::Entry ent2("bank2", "http://example.com/");
    db.addEntry(ent1)
        .addEntry(ent2);

    SECTION("delete non-existent record") {
        pml::Entry ent9999("bank9999", "http://example.com/");

        REQUIRE(! db.deleteEntry(ent9999));
        REQUIRE(db.allRecords().size() == 2U);
    }

    SECTION("delete record") {
        REQUIRE(db.deleteEntry(ent1));
        REQUIRE(db.allRecords().size() == 1U);
    }

    SECTION("delete all records") {
        REQUIRE(db.deleteEntry(ent1));
        REQUIRE(db.deleteEntry(ent2));
        REQUIRE(db.allRecords().size() == 0U);
    }
}

TEST_CASE("query entries") {
    pml::DB db;
    std::vector<pml::Entry> results;
    pml::Entry result;

    pml::Entry ent1("bank1", "http://example.com/");
    pml::Entry ent2("bank2", "http://example.com/");
    db.addEntry(ent1)
        .addEntry(ent2);

    SECTION("find non-existent record") {
        pml::Entry ent9999("bank9999", "http://example.com/");

        REQUIRE(! db.findEntry(ent9999, result));
        REQUIRE(db.query("nononono").size() == 0U);
    }

    SECTION("find record 1") {
        REQUIRE(db.findEntry(ent1, result));
        REQUIRE(result == ent1);
        REQUIRE(db.query(ent1.getName()).size() == 1U);
    }

    SECTION("find record 2") {
        REQUIRE(db.findEntry(ent2, result));
        REQUIRE(result == ent2);
        REQUIRE(db.query(ent2.getName()).size() == 1U);
    }

    SECTION("query by location") {
        results = db.query("http://example.com/");
        REQUIRE(results.size() == 2U);

        int what = pml::DB::QUERY_FLD_NAMLOC | pml::DB::QUERY_BY_KEY;
        results = db.query("http://example.com/", what);
        REQUIRE(results.size() == 0U);

        what = pml::DB::QUERY_FLD_NAMLOC | pml::DB::QUERY_BY_VALUE;
        results = db.query("http://example.com/", what);
        REQUIRE(results.size() == 2U);
    }

    SECTION("query by name 1") {
        results = db.query("bank1");
        REQUIRE(results.size() == 1U);
        REQUIRE(results[0] == ent1);

        int what = pml::DB::QUERY_FLD_NAMLOC | pml::DB::QUERY_BY_KEY;
        results = db.query("bank1", what);
        REQUIRE(results.size() == 0U);

        what = pml::DB::QUERY_FLD_NAMLOC | pml::DB::QUERY_BY_VALUE;
        results = db.query("bank1", what);
        REQUIRE(results.size() == 1U);
        REQUIRE(results[0] == ent1);
    }

    SECTION("query by name 2") {
        results = db.query("bank2");
        REQUIRE(results.size() == 1U);
        REQUIRE(results[0] == ent2);
    }

    SECTION("include extra data") {
        pml::Entry ent3("some", "thing");
        ent3.addMetaData("bank3", "http://example.com/");
        db.addEntry(ent3);

        int what = pml::DB::QUERY_FLD_NAMLOC
            | pml::DB::QUERY_FLD_REST
            | pml::DB::QUERY_BY_KEY
            | pml::DB::QUERY_BY_VALUE;

        results = db.query("bank3", what);
        REQUIRE(results.size() == 1U);

        results = db.query("http://example.com/", what);
        REQUIRE(results.size() == 3U);
    }
}

//
//  Serializtion Tests
//

static const char* one_entry = 
  "{ \"database\": [\n"
  "  { \"name\": \"Apple\",\n"
  "    \"location\": \"https://www.apple.com/\",\n"
  "    \"user\": \"fanboy\",\n"
  "    \"pass\": \"09f3278d55fa7a90\"\n"
  "  }\n"
  "  ]\n"
  "}";
static const char* edq_entry = 
  "{ \"database\": [\n"
  "  { \"name\": \"Apple\",\n"
  "    \"location\": \"https://www.apple.com/\",\n"
  "    \"user\": \"fanboy\",\n"
  "    \"pass\": \"09f327\\\"8d55fa7a90\"\n"
  "  }\n"
  "  ]\n"
  "}";


TEST_CASE("initialize from database string") {
    std::string dbstr;
    dbstr.append(one_entry);

    pml::DB db(pml::DB::dbFromSerializedString(dbstr));

    std::vector<pml::Entry> ents(db.allRecords());
    REQUIRE(ents.size() == 1U);
    REQUIRE(ents[0].getName() == "Apple");
    REQUIRE(ents[0].getLocation() == "https://www.apple.com/");
}

TEST_CASE("initialize from database bad string") {
    std::string dbstr;
    dbstr.append("this is not json");

    REQUIRE_THROWS_AS(pml::DB::dbFromSerializedString(dbstr),
                      const pml::SerializeException&);
}

TEST_CASE("initialize from database string embedded double quotes") {
    std::string dbstr;
    dbstr.append(edq_entry);

    pml::DB db(pml::DB::dbFromSerializedString(dbstr));

    std::vector<pml::Entry> ents(db.allRecords());
    REQUIRE(ents.size() == 1U);
    REQUIRE(ents[0].hasKey("pass"));
    REQUIRE(ents[0].getMetaValue("pass") == "09f327\"8d55fa7a90");
}

TEST_CASE("converting to database string") {
    pml::DB db;

    pml::Entry ent1("bank1", "http://example_one.com/");
    pml::Entry ent2("bank2", "http://example_two.com/");

    ent1.addMetaData("dog", "cat");
    ent2.addMetaData("desk", "table");

    db.addEntry(ent1).addEntry(ent2);

    std::string ser_form = pml::DB::serializedStringFromDb(db);

    INFO(ser_form);
    // WARN(ser_form);  // for debugging

    REQUIRE(ser_form.size() > 2);
    // kind of lame testing for what's in JSON string
    REQUIRE(ser_form.find("bank1") != std::string::npos);
    REQUIRE(ser_form.find("bank2") != std::string::npos);
    REQUIRE(ser_form.find("example_one") != std::string::npos);
    REQUIRE(ser_form.find("example_two") != std::string::npos);
    REQUIRE(ser_form.find("dog") != std::string::npos);
    REQUIRE(ser_form.find("cat") != std::string::npos);
    REQUIRE(ser_form.find("desk") != std::string::npos);
    REQUIRE(ser_form.find("table") != std::string::npos);
}

TEST_CASE("converting to database string with embedded double quote") {
    pml::DB db;

    pml::Entry ent1("bank1", "http://example_one.com/");
    pml::Entry ent2("bank2", "http://example_two.com/");

    ent1.addMetaData("dog", "cat");
    ent2.addMetaData("desk", "table\"chair");

    db.addEntry(ent1).addEntry(ent2);

    std::string ser_form = pml::DB::serializedStringFromDb(db);

    INFO(ser_form);

    // specifically look for embedded double quote
    REQUIRE(ser_form.find("table\\\"chair") != std::string::npos);
}

////
////////
////////////
//////////////// ----------  Entry tests

TEST_CASE("entry construction") {
    SECTION("empty") {
        pml::Entry ent;
        REQUIRE(ent.getName().size() == 0);
        REQUIRE(ent.getLocation().size() == 0);
    }
    SECTION("normal") {
        pml::Entry ent("A", "B");
        REQUIRE(ent.getName() == "A");
        REQUIRE(ent.getLocation() == "B");
    }
    SECTION("embedded double quote in key") {
        pml::Entry ent("A\"A", "B");
        REQUIRE(ent.getName() == "A\"A");
        REQUIRE(ent.getLocation() == "B");
    }
    SECTION("embedded double quote in value") {
        pml::Entry ent("A", "B\"B");
        REQUIRE(ent.getName() == "A");
        REQUIRE(ent.getLocation() == "B\"B");
    }
}

TEST_CASE("entry equality") {
    SECTION("empty") {
        pml::Entry entA;
        pml::Entry entB;
        REQUIRE(entA == entA);
        REQUIRE(entB == entB);
        REQUIRE(entA == entB);
    }

    SECTION("with keys") {
        pml::Entry entA("one", "two");
        pml::Entry entB("one", "two");
        REQUIRE(entA == entA);
        REQUIRE(entB == entB);
        REQUIRE(entA == entB);
    }

    SECTION("with extra data but still equal") {
        pml::Entry entA("one", "two");
        pml::Entry entB("one", "two");
        entB.addMetaData("animal", "cat");
        entB.addMetaData("food", "sub");
        REQUIRE(entA == entA);
        REQUIRE(entB == entB);
        REQUIRE(entA == entB);
    }

    SECTION("has keys") {
        pml::Entry entB("one", "two");
        entB.addMetaData("animal", "cat");
        REQUIRE(entB.hasKey("animal"));
        REQUIRE(! entB.hasKey("food"));
    }

    SECTION("has value") {
        pml::Entry entB("one", "two");
        entB.addMetaData("animal", "cat");
        REQUIRE(entB.hasValue("cat"));
        REQUIRE(! entB.hasKey("sub"));
    }
}
