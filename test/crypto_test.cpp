//    Copyright (C) 2016, 2022 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <string>
#include <iostream>

#include "catch.hpp"
#include <pml/crypto.hpp>

// 0 = mixed case letters
// 1 = digits
// 2 = special characters
// 3 = illegal characters
static std::size_t ctype[] = {
    /*      0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f */
    /* 0 */ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    /* 1 */ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    /* 2 */ 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    /* 3 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2,
    /* 4 */ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* 5 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2,
    /* 6 */ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* 7 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2,
    /* 8 */ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    /* 9 */ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    /* a */ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    /* b */ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    /* c */ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    /* d */ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    /* e */ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    /* f */ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3
};

struct TypeCounter {
    TypeCounter()      : which(-1), counts{0,0,0,0} {}
    TypeCounter(int w) : which(w),  counts{0,0,0,0} {}

    void operator() (char c) {
        int bads;

        if (which >= 0)
            bads = counts[which];

        ++counts[ctype[static_cast<std::size_t>(c)]];

        if (which >= 0 && counts[which] > bads)
            std::cout << "bad char=" << c << std::endl;
    }

    int which;
    int counts[4];
};


TEST_CASE("encrypt/decrypt") {
    std::string passphrase("mypassword");
    std::string data("once upon a time there was a dark cave in the middle of the woods.");

    pml::Crypto c;

    pml::encdata_t       cyphertext = c.encrypt(passphrase, data);
    pml::secstring_t decrypted_data = c.decrypt(passphrase, cyphertext);

    REQUIRE(data == decrypted_data);
}

TEST_CASE("encrypt/decrypt zero len passphrase") {
    std::string passphrase("");
    std::string data("once upon a time there was a dark cave in the middle of the woods.");

    pml::Crypto c;

    pml::encdata_t       cyphertext = c.encrypt(passphrase, data);
    pml::secstring_t decrypted_data = c.decrypt(passphrase, cyphertext);

    REQUIRE(data == decrypted_data);
}

TEST_CASE("encrypt/decrypt long passphrase") {
    std::string passphrase;
    passphrase.append("alkjdflajdfjaslkdjfalksjfl;kajsdfkljalsjflajsdfjaslj");
    passphrase.append("flajsdfalkjdfkljalkdfjlakjl;jaflksdjfklajdklfjalkjfd");
    passphrase.append("lkajdkl;fjakljfdlkajsdklfjaslkdfjlkajsdflk;jalk;dfjl");
    passphrase.append("jika;sjdfklajsdklfjaljdflkajdlkfjaslkdfjkla;dsjfl;kajfkljadskljfalksjf");
    std::string data("once upon a time there was a dark cave in the middle of the woods.");

    pml::Crypto c;

    pml::encdata_t       cyphertext = c.encrypt(passphrase, data);
    pml::secstring_t decrypted_data = c.decrypt(passphrase, cyphertext);

    REQUIRE(data == decrypted_data);
}

TEST_CASE("encrypt/decrypt wrong passphrase") {
    std::string passphrase("mypassword");
    std::string wrong_passphrase("not-mypassword");
    std::string data("once upon a time there was a dark cave in the middle of the woods.");

    pml::Crypto c;
    pml::encdata_t cyphertext = c.encrypt(passphrase, data);

    REQUIRE_THROWS(c.decrypt(wrong_passphrase, cyphertext));
}

TEST_CASE("genpw correct length") {
    pml::Crypto c;
    pml::Crypto::PwAttr attr;

    pml::secstring_t pass;

    SECTION("length 0") {
        pass = c.genPassword(0, attr);
        REQUIRE(pass.size() == 0);
    }

    SECTION("length 8") {
        pass = c.genPassword(8, attr);
        REQUIRE(pass.size() == 8);
    }

    SECTION("length 100") {
        pass = c.genPassword(100, attr);
        REQUIRE(pass.size() == 100);
    }
}

TEST_CASE("gen character password") {
    pml::Crypto c;
    pml::Crypto::PwAttr attr{-1,0,0};

    pml::secstring_t pass(c.genPassword(100, attr));

    TypeCounter cter;
    cter = std::for_each(pass.begin(), pass.end(), cter);

    REQUIRE(cter.counts[0] == 100);
    REQUIRE(cter.counts[1] == 0);
    REQUIRE(cter.counts[2] == 0);
    REQUIRE(cter.counts[3] == 0);
}

TEST_CASE("gen digit password") {
    pml::Crypto c;
    pml::Crypto::PwAttr attr{0,-1,0};

    pml::secstring_t pass(c.genPassword(100, attr));

    TypeCounter cter;
    cter = std::for_each(pass.begin(), pass.end(), cter);

    REQUIRE(cter.counts[0] == 0);
    REQUIRE(cter.counts[1] == 100);
    REQUIRE(cter.counts[2] == 0);
    REQUIRE(cter.counts[3] == 0);
}

TEST_CASE("gen special password") {
    pml::Crypto c;
    pml::Crypto::PwAttr attr{0,0,-1};

    pml::secstring_t pass(c.genPassword(100, attr));

    TypeCounter cter;
    cter = std::for_each(pass.begin(), pass.end(), cter);

    REQUIRE(cter.counts[0] == 0);
    REQUIRE(cter.counts[1] == 0);
    REQUIRE(cter.counts[2] == 100);
    REQUIRE(cter.counts[3] == 0);
}

TEST_CASE("gen all valid character") {
    pml::Crypto c;
    pml::Crypto::PwAttr attr{-1,-1,-1};

    pml::secstring_t pass(c.genPassword(100, attr));

    TypeCounter cter;
    cter = std::for_each(pass.begin(), pass.end(), cter);

    REQUIRE(cter.counts[3] == 0);
}

TEST_CASE("gen fixed number of letters") {
    pml::Crypto c;
    pml::Crypto::PwAttr attr{10,-1,-1};

    pml::secstring_t pass(c.genPassword(100, attr));

    TypeCounter cter;
    cter = std::for_each(pass.begin(), pass.end(), cter);

    REQUIRE(cter.counts[0] == 10);
    REQUIRE(cter.counts[3] == 0);
}

TEST_CASE("gen fixed number of digits") {
    pml::Crypto c;
    pml::Crypto::PwAttr attr{-1,10,-1};

    pml::secstring_t pass(c.genPassword(100, attr));

    TypeCounter cter;
    cter = std::for_each(pass.begin(), pass.end(), cter);

    REQUIRE(cter.counts[1] == 10);
    REQUIRE(cter.counts[3] == 0);
}

TEST_CASE("gen fixed number of specials") {
    pml::Crypto c;
    pml::Crypto::PwAttr attr{-1,-1,10};

    pml::secstring_t pass(c.genPassword(100, attr));

    TypeCounter cter;
    cter = std::for_each(pass.begin(), pass.end(), cter);

    REQUIRE(cter.counts[2] == 10);
    REQUIRE(cter.counts[3] == 0);
}

TEST_CASE("gen user passed specials") {
    pml::Crypto c;
    pml::Crypto::PwAttr attr{5,5,10,"$*"};

    pml::secstring_t pass(c.genPassword(100, attr));

    TypeCounter cter;
    // how many special chars in password
    cter = std::for_each(pass.begin(), pass.end(), cter);
    // how many of our special subset of chars in password
    auto special_subset_cnt = std::count_if(pass.begin(), pass.end(), [](char ch){return ch == '$' || ch == '*';});

    REQUIRE(cter.counts[2] == special_subset_cnt);
    REQUIRE(cter.counts[3] == 0);
}
