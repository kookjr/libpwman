//    Copyright (C) 2016, 2021 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Password Manager Library; a program to store
//    passwords.
//
//    Password Manager Library is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Password Manager Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Password Manager.  If not, see <http://www.gnu.org/licenses/>.

#include <string>

#include "catch.hpp"
#include <pml/types.hpp>
#include <pml/io.hpp>

TEST_CASE("read from file") {
    pml::rawdata_t file_contents;
    pml::IO io;

    file_contents = io.readFile("bin_test_data");
    REQUIRE(file_contents.size() > 0);
}

TEST_CASE("missing file") {
    pml::IO io;
    REQUIRE_THROWS_AS(io.readFile("no_file_named_this"), const pml::IOException&);
}

TEST_CASE("write to file") {
    pml::IO io;
    pml::rawdata_t vec{0x11, 0x12, 0x13, 0x14};
    io.writeFile("jk_remove_me", vec);
    // exception on failure
}

TEST_CASE("write file invalid path") {
    pml::IO io;
    pml::rawdata_t vec{0x11, 0x12, 0x13, 0x14};
    REQUIRE_THROWS_AS(io.writeFile("some/dir/does/not/exist/file", vec),
                      const pml::IOException&);
}
